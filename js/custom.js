$("#menu-lateral li").on("click", function() {
  $("#menu-lateral li").removeClass("active");
  $(this).addClass("active");
});

var owl = $('.jogadores-carousel');
owl.owlCarousel({
    loop:true,
    margin:0,
    dots:true,
    dotsEach: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    },
    onInitialized: function(event) {
      var item = $('.jogadores-carousel .owl-item.active')[1];
      $(item).addClass('active-slide');
    },
    onTranslated: function(event) {
      $('.jogadores-carousel .owl-item').removeClass('active-slide');
      $('.jogadores-carousel .owl-item.active').eq(1).addClass('active-slide');
    }
});

$('.videos-carousel').owlCarousel({
    loop:true,
    margin:0,
    dots:true,
    nav: true,
    dotsEach: true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        830:{
            items:2
        },
        1000:{
            items:4
        }
    }
});